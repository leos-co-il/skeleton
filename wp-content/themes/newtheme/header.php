<!DOCTYPE HTML>
<html <?php language_attributes(); ?> dir="ltr">
<?php global $my_lang,$sticky,$isMobile,$phone,$mail,$address,$socials,$logo,$c_seo,$c_page; $c_page = c_page(); ?>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>" charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php if ($c_page['page'] == 'search') { ?>
	   <meta name="robots" content="noindex, nofollow" />
	<?php } ?>
	<link rel="icon" href="<?php echo get_template_directory_uri().'/images/favicon.png'; ?>" type="image/png" />
	<?php if (0) { ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Amatica+SC:400,700&amp;subset=hebrew" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Assistant:300,400,600,700&amp;subset=hebrew" rel="stylesheet">
	<?php } ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/hover.css'; ?>" type="text/css"media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick/slick.css"/>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick/slick-theme.css"/>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<?php if ($c_seo) { echo $c_seo; } ?>
</head>

<body <?php body_class(); ?>>
<?php if ($c_page['is_tax']) {
    $term_id = $c_page['id'];
	$tax = $c_page['type'];
    $bbg = get_field('thebg',$tax.'_'.$term_id);
} else {
    $bbg = get_field('thebg');
} if (empty($bbg)) { $bbg = get_template_directory_uri().'/images/default.jpg'; } ?>
<div id="wrapper">
<header>
	<div class="container c18 flex aic">
		<div class="logo w15 pdgtb15">
			<a href="<?php echo get_option('home'); ?>" title="<?php bloginfo('name'); ?>">
				<?php if ($logo) { ?><img src="<?php echo $logo[url]; ?>" alt="<?php bloginfo('name'); ?>" /><?php } else { ?>
					<img class="w50" src="<?php echo get_template_directory_uri().'/images/leoslogo.png'; ?>" alt="<?php bloginfo('name'); ?>" />
				<?php }?>
			</a>
		</div>
		<nav id="MainNav">
		    <div id="MobNavBtn"><span></span><span></span><span></span></div>
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'depth' => '2', 'container_class' => 'main_menu', 'menu_class' => '') ); ?>
		</nav>
		<?php if($phone) {?>
			<h3 class="headerphone  hfs25 light flex aic"><span  class="mrgl10"><?php echo svg('svg/headphone');?></span><?php echo $phone[link];?></h3>
		<?php }?>
	</div>
</header>
<!-- טופס דביק-->
<?php if ($sticky) {?>
<div class="stickyform anim  pointer norpt nomobile " style="background-image:url('<?php echo $sticky;?>');"></div>
<?php }?>
<div class="hiddenform pdg30 pointer nomobile">
	<div class="X" ></div>
	<h2 class=" hfs25 center pdgb10 "><?php echo "השאיר פרטים ונחזור אליך בהדקם";?></h2>
	<?php echo do_shortcode('[contact-form-7 id="4" title="צור קשר"]'); ?>
</div>
<!-- סוף-->
<div id="content">
