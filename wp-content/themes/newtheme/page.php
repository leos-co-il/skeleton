<?php get_header(); ?>
<div id="ThePage">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="entry">
			<?php the_content(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
