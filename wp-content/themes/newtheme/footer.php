<?php global $my_lang,$phone,$mail,$address,$socials,$isMobile,$c_page; ?>
</div><!--#content end-->
<footer  >
    <?php $t1=get_field('part1_t','options');
          $t2=get_field('part2_t','options');
          $t3=get_field('part3_t','options');
          $t4=get_field('part4_t','options');
          $link=get_field('facebook_link','options');
      ?>
    <div class="container c15 flex  pdgb80 pdgt80 mblock">
        <div class="  ">
            <h2 class=" hfs23 mrgb25"><?php echo $t1;?></h2>
            <nav id="FooterNav">
                <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'depth' => '2', 'container_class' => 'main_menu', 'menu_class' => '') ); ?>
            </nav>
        </div>
        <div class="">
            <h2 class=" hfs23 mrgb25"><?php echo $t2;?></h2>
            <div class="fb-page w80" data-href="<?php echo $link;?>" data-tabs="header" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $link;?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $link;?>">Facebook</a></blockquote></div>
        </div>
        <div class="">
            <h2 class=" hfs23 mrgb25"><?php echo $t3;?></h2>
            <div class="w80 mw100">
                <nav id="FooterNav">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'depth' => '2', 'container_class' => 'main_menu', 'menu_class' => 'inrowf') ); ?>
                </nav>
            </div>
        </div>
        <div class="">
            <h2 class=" hfs23 mrgb25"><?php echo $t4;?></h2>
            <?php if ($phone) {?>
                <div class="flex  hfs17 mrgb25 fphone aic"><span class="mrgl10"><?php echo svg('headphone');?></span><?php echo $phone[txt];?></div>
            <?php }?>
        </div>
    </div>
    <?php $back=get_field('backtotop','options'); if ($back) {?>
        <div class="nomobile backtotop bgimg pointer bgscon" style="background-image:url('<?php echo $back;?>')" id="BackToTop"></div>
    <?php }?>
</footer>
<div id="leos">
	<a href="http://www.leos.co.il/" title="">
		<img src="<?php echo bloginfo('template_url');?>/images/leoslogo.png" alt="" />
		<span></span>
	</a>
</div>
<?php if ($socials) { ?>
<div id="FloatSocials">
    <?php echo $socials; ?>
</div>
<?php } ?>
<div id="MobBtns">
    <?php if ($phone) { ?>
        <a href="tel:<?php echo $phone[num]; ?>" class="fcc"><?php echo svg('svg/Mphone'); ?></a>
    <?php } ?>
    <?php if ($mail && 0) { ?>
        <a href="mailto:<?php echo $mail; ?>" class="fcc"><?php echo svg('svg/Mmail'); ?></a>
    <?php } ?>
    <?php if ($address) { ?>
        <a href="<?php echo $address[waze]; ?>" class="fcc"><?php echo svg('svg/Mloc'); ?></a>
    <?php } ?>
</div>
</div> <!-- close wrapper -->
<?php wp_footer(); ?>
</body>
</html>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1840068962951769&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
