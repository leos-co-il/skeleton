<?php get_header();
$queried_object = get_queried_object();
$term_id = $queried_object->term_id; ?>
<div id="ArchPage">
	<?php $top=get_field('pages_topimage',$queried_object) ? get_field('pages_topimage',$queried_object) : get_field('default','options');  ?>
		<div class="topimage bgimg relative  " style="  background-image:url('<?php echo $top;?>')">
			<div class="h100 w100 flex aife fcc mmnh350" style="min-height:500px;background-color:rgba(0, 32, 72,0.3);">
				<h1 class="hfs100 wcolor  bold center pdgb80"><?php echo single_cat_title();?></h1>
			</div>
		</div>
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') )
		{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

		<div class="postsbox inrowf">
			<?php while (have_posts()) : the_post(); ?>
			<div class="onepost col3">
			    <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			    <div class="entry">
			    	<?php the_excerpt(); ?>
			    </div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
