(function() {
	tinymce.PluginManager.add('y_video_button', function( editor, url ) {
		editor.addButton( 'y_video_button', {
			title: 'YouTube Video',
			icon: 'icon youtube-video',
            onclick: function() {
            editor.windowManager.open( {
            	title: 'Embed YouTube Video By Leos',
            	body: [
                    {
            			type: 'textbox',
            			name: 'src',
            			label: 'Youtube URL',
            			value: '',
            			multiline: false,
            			minWidth: 300
            		},
            		{
            			type: 'checkbox',
            			name: 'autoplay',
            			label: 'Autoplay?',
            		},
                    {
            			type: 'checkbox',
            			name: 'loop',
            			label: 'Video loop?',
            		},
            		{
            			type: 'checkbox',
            			name: 'rel',
            			label: 'Related videos?',
            		},
                    {
            			type: 'checkbox',
            			name: 'mute',
            			label: 'Mute?',
            		},
            	],
            	onsubmit: function( e ) {
            	    var scr = e.data.src;
            	    var ap = e.data.autoplay;
            	    var loop = e.data.loop;
            	    var rel = e.data.rel;
            	    var mute = e.data.mute;
            	    if (ap) { ap = 1; } else { ap = 0; }
            	    if (loop) { loop = 1; } else { loop = 0; }
            	    if (rel) { rel = 1; } else { rel = 0; }
            	    if (mute) { mute = 1; } else { mute = 0; }
            		editor.insertContent( '[YouTube src="' + scr + '" ap="' + ap + '" rel="' + rel + '" loop="' + loop + '" mute="' + mute + '"]');
            	}
            });
            }
		});
	});
})();

/*
{
type: 'listbox',
name: 'listboxName',
label: 'List Box',
'values': [
{text: 'Option 1', value: '1'},
{text: 'Option 2', value: '2'},
{text: 'Option 3', value: '3'}
]
}
*/
