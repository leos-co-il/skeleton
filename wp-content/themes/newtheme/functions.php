<?php global $my_lang,$phone,$mail,$address,$socials,$logo,$cuser,$c_seo,$options,$isMobile,$c_page;
$cuser = get_current_user_role();
$options = get_option('leos_set');
$isMobile = wp_is_mobile();
if (!empty($options['set_seo'])) {
    $c_seo = $options['set_seo'];
}
if (ICL_LANGUAGE_CODE == 'ICL_LANGUAGE_CODE') {
    $my_lang = 'he';
} else {
    $my_lang = ICL_LANGUAGE_CODE;
}
if (function_exists('get_field') && !is_admin()) {
    $mail = get_field('mail','options');
    $logo = get_field('the_logo','options');
    $address = get_field('address','options');
    $ph = get_field('phone','options');
    if ($ph) {
        $phone["txt"] = $ph;
        $phone["num"] = preg_replace("/[^A-Za-z0-9]/", "",$ph);
        $phone["tel"] = 'tel:'.$phone["num"];
        $phone["link"] = '<a href="tel:'.$phone["num"].'">'.$ph.'</a>';
    }
    if ($address) {
        $addr = explode(',',$address["address"]);
        $address["name"] = $addr[0].', '.$addr[1];
        $address["gmlink"] = 'http://www.google.com/maps/place/'.$address["lat"].','.$address["lng"];
        $address["waze"] = 'waze://?q='.$address["name"];
    }
    $socials = get_field('socials','options');
    if ($socials) {
        foreach($socials as $social) {
            $soc_name = explode('.',remove_ctags($social[link]))[0];
            $temp .= '<a href="'.$social[link].'" class="'.$soc_name.'_icon anim fcc" style="background-color:'.$social[color].';" title="'.$soc_name.'" target="_new">'.svg($social[icon]).'</a>';
        }
        $socials = $temp;
    }
}
function remove_ctags($url) {
   $disallowed = array('http://', 'https://', 'www.', 'WWW.');
   foreach($disallowed as $d) {
      if(strpos($url, $d) === 0) {
         $url = str_replace($d, '', $url);
      }
   }
   return $url;
}
automatic_feed_links();

if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"), false);
    wp_enqueue_script('jquery');
}

function removeHeadLinks() {
    remove_action( 'wp_head', 'rsd_link' );
    remove_action( 'wp_head', 'wlwmanifest_link' );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
    remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
    remove_action( 'wp_head', 'rel_canonical');
}

add_action('init', 'removeHeadLinks');

remove_action('welcome_panel', 'wp_welcome_panel');

function wpt_create_widget( $name, $id, $description ) {
    if (function_exists('register_sidebar')) {
        register_sidebar(array(
            'name' => __($name),
            'id' => $id,
            'description' => __($description),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h2>',
            'after_title' => '</h2>'
        ));
    }
};

wpt_create_widget('Sidebar Widgets', 'sidebar-widgets', 'These are widgets for the sidebar.' );

include_once(get_template_directory() . '/login/leoslogin.php');

function register_my_menu() {
    register_nav_menu('header-menu', __('Header Menu'));
    register_nav_menu('footer-menu', __('Footer Menu'));
    register_nav_menu('sidebar-menu', __('Sidebar Menu'));
}

add_action('init', 'register_my_menu');

add_theme_support( 'post-thumbnails' );

add_image_size( 'thumbnail', 250, 250, true );
add_image_size( 'medium', 550, 550);
add_image_size( 'large', 1024, 1024, true );
add_image_size( 'full-hd', 1920, 1080 );
add_image_size( 'hd', 1280, 720 );
add_image_size( 'mobile', 800, 9999 );
add_image_size( 'medium_large', 760, 9999 );

add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
function wpshout_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'full-hd' => 'Full HD',
        'hd' => 'HD 720p',
        'mobile' => 'Mobile',
        'medium_large' => 'Medium Large',
    ) );
}

function custom_excerpt_length($length) {
    return 20;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

function theme_name_scripts() {
    wp_enqueue_script('custom3-script', get_template_directory_uri() . '/js/myjs.js', false, '', true);
    wp_enqueue_script('custom4-script', get_template_directory_uri() . '/js/cfmap.js', false, '', true);
}

add_action('wp_enqueue_scripts', 'theme_name_scripts');
add_theme_support('post-formats', array('gallery', 'video'));


add_action('admin_head', 'custom_admin_css');
function custom_admin_css() {
	wp_enqueue_style('category-style', get_template_directory_uri() . '/css/custom_admin.css');
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'הגדרות האתר',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Home Settings',
		'menu_title'	=> 'Home',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Settings',
        'menu_title'	=> 'Settings',
        'parent_slug'	=> 'theme-general-settings',
    ));

}

$result = add_role( 'leos_client', __(

'Leos Client' ),

array(
  'activate_plugins' => false,
  'delete_others_pages' => true,
  'delete_others_posts' => true,
  'delete_pages' => true,
  'delete_posts' => true,
  'delete_private_pages' => true,
  'delete_private_posts' => true,
  'delete_published_pages' => true,
  'delete_published_posts' => true,
  'edit_dashboard' => true,
  'edit_others_pages' => true,
  'edit_others_posts' => true,
  'edit_pages' => true,
  'edit_posts' => true,
  'edit_private_pages' => true,
  'edit_private_posts' => true,
  'edit_published_pages' => true,
  'edit_published_posts' => true,
  'edit_theme_options' => true,
  'export' => false,
  'import' => false,
  'list_users' => true,
  'manage_categories' => true,
  'manage_links' => true,
  'manage_options' => true,
  'moderate_comments' => true,
  'promote_users' => false,
  'publish_pages' => true,
  'publish_posts' => true,
  'read_private_pages' => true,
  'read_private_posts' => true,
  'read' => true,
  'remove_users' => false,
  'switch_themes' => false,
  'upload_files' => true,
  'update_core' => false,
  'update_plugins' => false,
  'update_themes' => false,
  'install_plugins' => false,
  'install_themes' => false,
  'delete_themes' => false,
  'delete_plugins' => false,
  'edit_plugins' => false,
  'edit_themes' => false,
  'edit_files' => false,
  'edit_users' => false,
  'add_users' => false,
  'create_users' => false,
  'delete_users' => false,
  'unfiltered_html' => true,
  'manage_woocommerce' => true,
  'manage_woocommerce_orders'	=> true,
  'manage_woocommerce_coupons' => true,
  'manage_woocommerce_products' => true,
  'edit_products' => true,
  'edit_others_products' => true,
  'copy_posts' => true,
  'view_woocommerce_reports' => true,
  'edit_product' => true,
  'read_product' => true,
  'delete_product' => true,
  'publish_products' => true,
  'read_private_products' => true,
  'delete_products' => true,
  'delete_private_products' => true,
  'delete_published_products' => true,
  'delete_others_products' => true,
  'edit_private_products' => true,
  'edit_published_products' => true,
  'manage_product_terms' => true,
  'edit_product_terms' => true,
  'delete_product_terms' => true,
  'assign_product_terms' => true,
  'edit_shop_order' => true,
  'read_shop_order' => true,
  'delete_shop_order' => true,
  'edit_shop_orders' => true,
  'edit_others_shop_orders' => true,
  'publish_shop_orders' => true,
  'read_private_shop_orders' => true,
  'delete_shop_orders' => true,
  'delete_private_shop_orders' => true,
  'delete_published_shop_orders' => true,
  'delete_others_shop_orders' => true,
  'edit_private_shop_orders' => true,
  'edit_published_shop_orders' => true,
  'manage_shop_order_terms' => true,
  'edit_shop_order_terms' => true,
  'delete_shop_order_terms' => true,
  'assign_shop_order_terms' => true,
  'edit_shop_coupon' => true,
  'read_shop_coupon' => true,
  'delete_shop_coupon' => true,
  'edit_shop_coupons' => true,
  'edit_others_shop_coupons' => true,
  'publish_shop_coupons' => true,
  'read_private_shop_coupons' => true,
  'delete_shop_coupons' => true,
  'delete_private_shop_coupons' => true,
  'delete_published_shop_coupons' => true,
  'delete_others_shop_coupons' => true,
  'edit_private_shop_coupons' => true,
  'edit_published_shop_coupons' => true,
  'manage_shop_coupon_terms' => true,
  'edit_shop_coupon_terms' => true,
  'delete_shop_coupon_terms' => true,
  'assign_shop_coupon_terms' => true,
  'edit_shop_webhook' => true,
  'read_shop_webhook' => true,
  'delete_shop_webhook' => true,
  'edit_shop_webhooks' => true,
  'edit_others_shop_webhooks' => true,
  'publish_shop_webhooks' => true,
  'read_private_shop_webhooks' => true,
  'delete_shop_webhooks' => true,
  'delete_private_shop_webhooks' => true,
  'delete_published_shop_webhooks' => true,
  'delete_others_shop_webhooks' => true,
  'edit_private_shop_webhooks' => true,
  'edit_published_shop_webhooks' => true,
  'manage_shop_webhook_terms' => true,
  'edit_shop_webhook_terms' => true,
  'delete_shop_webhook_terms' => true,
  'assign_shop_webhook_terms' => true,
)
);

function get_current_user_role($type = 'role') {
	global $wp_roles;
	$current_user = wp_get_current_user();
    if ($type == 'role') {
	    $roles = $current_user->roles;
        $role = array_shift($roles);
        return isset($wp_roles->role_names[$role]) ? translate_user_role($wp_roles->role_names[$role] ) : false;
    } elseif ($type == 'id') {
        $roles = $current_user->ID;
        return $roles;
    } elseif ($type == 'mail') {
        $roles = $current_user->data->user_email;
        return $roles;
    }
}

function remove_admin_menu_links(){
    $user = get_current_user_role();
    if( $user == 'Leos Client' ) {
        remove_menu_page('tools.php');
        remove_menu_page('plugins.php');
	    remove_menu_page('options-general.php');
	    remove_menu_page('edit.php?post_type=fancy-gallery');
	    remove_menu_page('edit.php?post_type=gallery');
	    remove_menu_page('edit.php?post_type=acf-field-group');
	    remove_menu_page('leos_settings');
	    remove_menu_page('wpcf7' );
	    remove_menu_page('itsec');
        remove_menu_page('themes.php');
        remove_submenu_page( 'profile.php', 'user-new.php' );
        remove_submenu_page( 'index.php', 'update-core.php' );
        remove_menu_page('sitepress-multilingual-cms/menu/languages.php');
        add_menu_page( 'menus', 'תפריטים', 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 30 );
    } else {
        remove_submenu_page( 'themes.php', 'nav-menus.php' );
        remove_submenu_page( 'themes.php', 'widgets.php' );
        remove_submenu_page( 'themes.php', 'customize.php' );
        remove_menu_page('edit.php?post_type=fancy-gallery');
        add_menu_page( 'menus', 'תפריטים', 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 30 );
        add_menu_page( 'menus', "וידג'טים", 'manage_options', 'widgets.php', '', 'dashicons-welcome-widgets-menus', 31 );
    }
}
add_action('admin_menu', 'remove_admin_menu_links', 9999);

remove_theme_support( 'genesis-admin-menu' );

function remove_customize_page(){
	global $submenu;
	unset($submenu['themes.php'][6]);
}
add_action( 'admin_menu', 'remove_customize_page');

function auto_settings($hook) {
    wp_enqueue_script( 'admin_script', get_template_directory_uri() . '/js/admin.js' );
    //echo $hook;
    $runjspages = array(
        "security_page_toplevel_page_itsec_settings",
        "security_page_toplevel_page_itsec_advanced",
        "settings_page_gallery-options",
        "options-general.php",
        "options-reading.php",
        "options-discussion.php",
        "options-permalink.php",
        "users.php",
        "profile.php",
        "settings_page_swpsmtp_settings",
        "settings_page_tinymce-advanced",
        "toplevel_page_wpseo_dashboard",
        "seo_page_wpseo_titles",
        "seo_page_wpseo_advanced",
        "toplevel_page_leos_settings",
        "toplevel_page_wpcf7",
        "plugins.php",
        "update-core.php",
        "about.php",
        "options-media.php",
        "post.php",
    );
    if (!in_array($hook, $runjspages)) {
        //echo $hook;
        return;
    }
    global $options;
    if (!$options['set_done']) {
        wp_enqueue_script( 'first_settings_script', get_template_directory_uri() . '/js/autoconfig.js' );
    }
}

add_action( 'admin_enqueue_scripts', 'auto_settings' );

class LeosSettings
{
    private $options;

    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    public function add_plugin_page()
    {
        add_menu_page(
            'Leos Settings',
            'Leos Settings',
            'manage_options',
            'leos_settings',
            array( $this, 'create_admin_page' )
        );
    }

    public function create_admin_page()
    {
        $this->options = get_option( 'leos_set' );
        ?>
        <div class="wrap">
            <h2>Leos Settings</h2>
            <form method="post" action="options.php">
            <?php
                settings_fields( 'leos_option_group' );
                do_settings_sections( 'leos_settings' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    public function page_init()
    {
        register_setting(
            'leos_option_group',
            'leos_set',
            array( $this, 'sanitize' )
        );

        add_settings_section(
            'setting_section_id',
            'Leos Settings',
            array( $this, 'print_section_info' ),
            'leos_settings'
        );

        add_settings_field(
            'set_done',
            'Disable Auto Config:',
            array( $this, 'set_done_callback' ),
            'leos_settings',
            'setting_section_id'
        );

        add_settings_field(
            'set_products',
            'Add CPT Products:',
            array( $this, 'set_products_callback' ),
            'leos_settings',
            'setting_section_id'
        );

        add_settings_field(
            'set_projects',
            'Add CPT Projects:',
            array( $this, 'set_projects_callback' ),
            'leos_settings',
            'setting_section_id'
        );

        add_settings_field(
            'set_recs',
            'Add CPT Recommendations:',
            array( $this, 'set_recs_callback' ),
            'leos_settings',
            'setting_section_id'
        );

        add_settings_field(
            'set_cncpt',
            'CPT With Custom Name:',
            array( $this, 'set_cncpt_callback' ),
            'leos_settings',
            'setting_section_id'
        );

        add_settings_field(
            'gmap_key',
            'Google Map Key:',
            array( $this, 'gmap_key_callback' ),
            'leos_settings',
            'setting_section_id'
        );

        add_settings_field(
            'set_seo',
            'Before head close tag:',
            array( $this, 'set_seo_callback' ),
            'leos_settings',
            'setting_section_id'
        );
    }

    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['set_cncpt'] ) )
            $new_input['set_cncpt'] = $input['set_cncpt'];

        if( isset( $input['gmap_key'] ) )
            $new_input['gmap_key'] = $input['gmap_key'];

        if( isset( $input['set_done'] ) )
            $new_input['set_done'] = absint( $input['set_done'] );

        if( isset( $input['set_products'] ) )
            $new_input['set_products'] = absint( $input['set_products'] );

        if( isset( $input['set_projects'] ) )
            $new_input['set_projects'] = absint( $input['set_projects'] );

        if( isset( $input['set_recs'] ) )
            $new_input['set_recs'] = absint( $input['set_recs'] );

        if( isset( $input['auto_config'] ) )
            $new_input['auto_config'] = sanitize_text_field( $input['auto_config'] );

        if( isset( $input['set_seo'] ) )
            $new_input['set_seo'] = $input['set_seo'];

        return $new_input;
    }

    public function print_section_info()
    {
        global $options;

        if (!$options['set_done']) {
            echo '<input type="button" id="FPON" class="lsp_btns" value="01 Start Plugins" style="cursor:pointer;margin:0 5px;">';
            echo '<input type="button" id="lazy_ass" class="lsp_btns" value="02 Start Auto Config" style="cursor:pointer;margin:0 5px;">';
            echo '<input type="button" id="UPDATEP" class="lsp_btns" value="03 Update All" style="cursor:pointer;margin:0 5px;">';
        } else {
            echo '<input type="button" id="FPON" value="01 Start Plugins" style="cursor:pointer;margin:0 5px;" disabled>';
            echo '<input type="button" id="lazy_ass" value="02 Start Auto Config" style="margin:0 5px;" disabled>';
            echo '<input type="button" id="UPDATEP" value="03 Update All" style="cursor:pointer;margin:0 5px;" disabled>';
        }

    }
    public function set_seo_callback()
    {
        global $options;
        ?>
        <textarea id="set_seo" dir="ltr" name="leos_set[set_seo]" style="width:400px;height:200px;"><?php echo $options['set_seo']; ?></textarea>
        <?php
    }
    public function set_done_callback()
    {
        global $options;
    	?>
    	<input type="checkbox" id="set_done" name="leos_set[set_done]" <?php checked( $options['set_done'], 1 ); ?> value='1'>
    	<?php
    }
    public function set_products_callback()
    {
        global $options;
        ?>
        <input type="checkbox" id="set_products" name="leos_set[set_products]" <?php checked( $options['set_products'], 1 ); ?> value='1'>
        <?php
    }
    public function set_projects_callback()
    {
        global $options;
        ?>
        <input type="checkbox" id="set_projects" name="leos_set[set_projects]" <?php checked( $options['set_projects'], 1 ); ?> value='1'>
        <?php
    }
    public function set_recs_callback()
    {
        global $options;
        ?>
        <input type="checkbox" id="set_recs" name="leos_set[set_recs]" <?php checked( $options['set_recs'], 1 ); ?> value='1'>
        <?php
    }
    public function set_cncpt_callback()
    {
        global $options;
        ?>
        <input type="text" id="set_cncpt" name="leos_set[set_cncpt]" value='<?php echo $options['set_cncpt']; ?>' style="width:400px;">
        <?php
    }
    public function gmap_key_callback()
    {
        global $options;
        ?>
        <input type="text" id="gmap_key" name="leos_set[gmap_key]" value='<?php echo $options['gmap_key']; ?>' style="width:400px;">
        <?php
    }
}

if( is_admin() && $cuser != 'Leos Client' ) {
    $my_settings_page = new LeosSettings();
}

function wpcontent_svg_mime_type( $mimes = array() ) {
  $mimes['svg']  = 'image/svg+xml';
  $mimes['svgz'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'wpcontent_svg_mime_type' );

function ignore_upload_ext($checked, $file, $filename, $mimes){

	//we only need to worry if WP failed the first pass
	if(!$checked['type']){
		//rebuild the type info
		$wp_filetype = wp_check_filetype( $filename, $mimes );
		$ext = $wp_filetype['ext'];
		$type = $wp_filetype['type'];
		$proper_filename = $filename;

		//preserve failure for non-svg images
		if($type && 0 === strpos($type, 'image/') && $ext !== 'svg'){
			$ext = $type = false;
		}

		//everything else gets an OK, so e.g. we've disabled the error-prone finfo-related checks WP just went through. whether or not the upload will be allowed depends on the <code>upload_mimes</code>, etc.

		$checked = compact('ext','type','proper_filename');
	}

	return $checked;
}
add_filter('wp_check_filetype_and_ext', 'ignore_upload_ext', 10, 4);

add_filter('acf/settings/show_admin', 'my_acf_show_admin');

function my_acf_show_admin( $show ) {
    global $cuser;
    if( $cuser == 'Leos Client' ) {
        $can = 0;
    } else {
        $can = 1;
    }
    return $can;
}

/**
 * Custom functions
 */
function trans($array) {
    global $my_lang;
    if (!array_key_exists($my_lang, $array)) {
        if ($array['en']) {
            return $array['en'];
        }
        else {
            return $array['he'];
        }
    }
    else {
        return $array[$my_lang];
    }
}

function get_text($txt, $string) {
    if (empty($txt) && !is_array($string)) {
        $txt = $string;
    }
    elseif (empty($txt)) {
        $txt = trans($string);
    }
    return $txt;
}

function get_field_from($slug, $from = '') {
    if (is_object($from)) {
        if (array_key_exists('taxonomy', $from)) {
            $sel = 'term';
        }
        else {
            $sel = 'post';
        }
    }
    elseif (strpos($from, '_') !== FALSE) {
        $sel = 'term';
    }
    else {
        $sel = $from;
    }
    switch ($sel) {
        case 'term' :
            $field = get_field($slug, $from);
            break;
        case 'options' :
            $field = get_field($slug, 'options');
            break;
        default :
            $field = get_field($slug, $from);
    }
    return $field;
}

function img_empty($def = '') {
    $imgs_dir = get_dir('images/');
    if (strpos($def, 'http') !== FALSE || strpos($def, 'www') !== FALSE) {
        $url = $def;
    }
    elseif ($def) {
        $url = $imgs_dir . $def;
    }
    else {
        $url = $imgs_dir . 'defthumb.jpg';
    }
    return $url;
}

function img_size($img, $size) {
    if (is_array($img)) {
        if ($size == 'url' || $size == 'full') {
            $url = $img['url'];
        }
        elseif (empty($size)) {
            $url = $img['sizes']['medium'];
        }
        else {
            $url = $img['sizes'][$size];
        }
    }
    else {
        $url = $img;
    }
    return $url;
}

function get_object($type = 'text', $slug, $field_empty = '', $from = '', $size = 'medium') {
    $field = get_field_from($slug, $from);
    $fe = (empty($field)) ? true : false;
    switch ($type) {
        case 'svg' :
            $class = $from;
            $from = $field_empty;
            $alt = $size;
            $field = get_field_from($slug, $from);
            $result = svg($field, $class, $alt);
            break;
        case 'img' :
            if ($fe) {
                $img_url = img_empty($field_empty);
            }
            else {
                $img_url = img_size($field, $size);
            }
            $result = $img_url;
            break;
        case 'bg' :
            if ($fe) {
                $img_url = img_empty($field_empty);
            }
            else {
                $img_url = img_size($field, $size);
            }
            $result = 'background-image:url(' . $img_url . ');';
            break;
        default :
            $result = get_text($field, $field_empty);
    }
    return $result;
}

function video_image($id){
    $ch = curl_init('http://vimeo.com/api/v2/video/'.$id.'.php');
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
    $a = curl_exec($ch);
    $hash = unserialize($a);
    return $hash[0]["thumbnail_medium"];
}

function iframe($iframe, $params = array(), $class='iframe-'){
    if (strpos($iframe, 'vimeo') === FALSE) {
        if (strpos($iframe, 'src') !== FALSE) {
            preg_match('/src="(.+?)"/', $iframe, $matches);
            $src = $matches[1];
        } elseif (strpos($iframe, 'youtu.be') !== FALSE || strpos($iframe, 'watch?v=') !== FALSE || strpos($iframe, '?feature=oembed') !== FALSE) {
            if (preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $iframe, $matches)) {
                $youtube_id = $matches[1];
            }
            $src = 'https://www.youtube.com/embed/'.$youtube_id;
        }
    } else {
        $src = $iframe;
    }
    $isYoutube = strpos($src, 'youtube');
    $isVimeo = strpos($src, 'vimeo');
    $htmlClass = $class;
    $return = $params['return'];
    unset($params['return']);
    if (empty($return)) { $return = 'iframe'; }

    $v_def_params = array(
        'api' => true,
        'loop' => 0,
        'title' => true,
        'color' => 'white',
        'width' => '',
        'xhtml' => false,
        'byline' => true,
        'height' => '',
        'portrait' => true,
        'callback' => '',
        'autoplay'  => false,
        'maxwidth' => '',
        'maxheight' => '',
        'player_id'	=> '',
        'autopause' => true,
    );

    $y_def_params = array(
        'fs'             => 0,
        'rel'            => 0,
        'loop'           => 0,
        'color'          => 'white',
        'theme'          => 'light',
        'playlist'       => '',
        'showinfo'       => 0,
        'autoplay'       => 1,
        'controls'       => 2,
        'autohide'       => 1,
        'disablekb'      => 1,
        'enablejsapi'    => 1,
        'modestbranding' => 0,
    );

    if ($isVimeo){
        $type = 'vimeo';
        foreach($v_def_params as $key => $val) {
            if (!array_key_exists($key,$params)) {
                $params[$key] = $val;
            }
        }
        $params = array_filter($params);

        $the_id  = $vimeoID = str_replace('video/', '',substr(parse_url($src, PHP_URL_PATH), 1));
        if ($return == 'array') {
            $thumb = video_image($vimeoID);
        }
        $the_src = $new_src = add_query_arg($params, $src);
        $the_iframe = $vimeoIframe = '<iframe src="'.$new_src.'" class="'.$htmlClass.'-vimeo" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
    if ($isYoutube){
        $type = 'youtube';
        foreach($y_def_params as $key => $val) {
            if (!array_key_exists($key,$params)) {
                $params[$key] = $val;
            }
        }
        $params = array_filter($params);

        $the_id  = $youTubeID = array_pop(explode('/',explode('?',$src)[0]));
        $thumb = 'https://img.youtube.com/vi/'.$youTubeID.'/0.jpg';
        $the_src = $new_src = add_query_arg($params, $src);
        $the_iframe = $youtubeIframe = '<iframe src="'.$new_src.'" class="'.$htmlClass.'-youtube" frameborder="0" allowfullscreen></iframe>';
    }

    switch ($return) {
        case 'id':
            return $the_id;
            break;

        case 'url':
            return $the_src;
            break;

        case 'src':
            return $the_src;
            break;

        case 'data':
            echo 'data-type="'.$type.'" data-code="'.$the_id.'"';
            break;

        case 'array':
            $the_return = array(
                'id'  => $the_id,
                'src'  => $the_src,
                'type' => $type,
                'thumb' => $thumb,
                'iframe' => $the_iframe,
            );
            return $the_return;
            break;

        default:
            return $the_iframe;
            break;
    }
}

function langs_select($args = 'flag')
{
    $langs = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
    $flags_dir = get_dir('/images/flags/');

    if ($args != 'flag' && strpos($args, ',') !== FALSE) {
        $args = explode(',', $args);
    }
    else {
        $args = array($args);
    }

    $h_flag = 0;
    $h_name = 0;
    $h_short = 0;
    $h_trans = 0;
    $h_select = 0;
    $show_active = 1;

    if (in_array('name', $args)) {
        $h_name = 1;
    }
    if (in_array('short', $args)) {
        $h_short = 1;
    }
    if (in_array('trans', $args)) {
        $h_trans = 1;
    }
    if (in_array('select', $args)) {
        $h_select = 1;
    }
    if (in_array('hide_active', $args) && !$h_select) {
        $show_active = 0;
        $args = array_diff($args, array('hide_active'));
    }
    if (in_array('flag', $args) || ($h_select && count($args) == 1)) {
        $h_flag = 1;
    }

    foreach ($langs as $key => $val) {

        $active = $val['active'];
        if ( ($active && $show_active) || !$active) {
            $url = $val['url'];
            $tag = explode('-', $val['tag'])[1];
            $code = $val['code'];
            $native_name = $val['native_name'];
            $translated_name = $val['translated_name'];

            if ($active) {
                $cls = ' active';
            }
            else {
                $cls = '';
            }

            if ($h_short) {
                if ($h_trans || $code == 'en') {
                    $name = ucfirst($code);
                }
                elseif ($code == 'he') {
                    $name = substr($native_name, 0, 4);
                }
                else {
                    $name = substr($native_name, 0, 3);
                }
            }
            else {
                if ($h_name) {
                    $name = $native_name;
                }
                elseif ($h_trans) {
                    $name = $translated_name;
                }
            }

            $start = '<a id="Lang-' . $code . '" href="' . $url . '" class="lang flex aic' . $cls . '">';
            unset($inside);
            $end = '</a>';

            if ($h_flag) {
                $flag = '<img src="' . $flags_dir . strtolower($tag) . '.png' . '" alt=""/>';
                $inside = $flag;
            }

            if ($name) {
                $inside .= '<span>' . $name . '</span>';
            }

            if ($h_select) {
                $temp .= '<li class="select-item' . $cls . '">' . $start . $inside . $end . '</li>';
            }
            else {
                $temp .= $start . $inside . $end;
            }
        }
    }
    if ($h_select) {
        $script = '<script>$(document).ready(function(){var e=$("#LoesLangsSelect ul");e.height(e.outerHeight()),e.removeClass("dropped");var a=e.find("li.active");a.find("a").attr("href","javascript:void(0);"),e.prepend(a),$("#LoesLangsSelect").removeAttr("class"),$("#LoesLangsSelect ul li a").click(function(a){e.hasClass("dropped")?(e.prepend($(this).parent()),e.removeClass("dropped")):(a.preventDefault(),e.addClass("dropped"))})});</script>';
        if ($h_flag) {
            $ul = '<ul class="langs_list dropped hflags">';
        }
        else {
            $ul = '<ul class="langs_list dropped">';
        }
        $temp = '<div id="LoesLangsSelect" class="overh">' . $ul . $temp . '</ul><svg height=32px id=Capa_1 style="enable-background:new 0 0 31.999 32"version=1.1 viewBox="0 0 31.999 32"width=31.999px x=0px xml:space=preserve xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink y=0px><g><path d="M31.92,5.021l-14.584,22.5c-0.089,0.139-0.241,0.223-0.406,0.229c-0.004,0-0.009,0-0.014,0   c-0.16,0-0.311-0.076-0.404-0.205L0.096,5.044C-0.015,4.893-0.031,4.69,0.054,4.523S0.312,4.25,0.5,4.25h31   c0.183,0,0.352,0.101,0.438,0.261C32.025,4.67,32.019,4.868,31.92,5.021z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>' . $script;
    }
    $return = $temp;

    return $return;
}

function trunc($phrase, $max_words) {
    $phrase = strip_shortcodes($phrase);
    $phrase = strip_tags($phrase);
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $max_words && $max_words > 0)
      $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
    return $phrase;
}

function truncReverse($phrase, $offset) {
    $phrase = strip_shortcodes($phrase);
    $phrase = strip_tags($phrase);
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $offset && $offset > 0){
        $phrase = implode(' ',array_slice($phrase_array, $offset));
        return $phrase;
    }

    return '';

}

function get_img($file = '',$class = '',$alt = '') {
    $img = get_dir('images/'.$file);
    $img = '<img src="'.$img.'" class="'.$class.'" alt="'.$alt.'"/>';
    return $img;
}
function get_thumb_url($size = 'medium',$id = '',$def = '',$alt = '0') {
    if ($id == '') { $id = $post->ID; }
    if (is_object($id)) {
        $thumb = get_field('thumb',$id->taxonomy.'_'.$id->term_id);
        $url = $thumb["sizes"][$size];
    } elseif (is_array($id)) {
        $thumb = get_field('thumb',$id["tax"].'_'.$id["id"]);
        $url = $thumb["sizes"][$size];
    } else {
        $thumbnail_id = get_post_thumbnail_id($id);
        $url = wp_get_attachment_image_src($thumbnail_id, $size)[0];
    }
    if (empty($url)) {
        if ($def) {
            $url = get_dir('images/'.$def);
        } else {
            $url = get_dir('images/defthumb.jpg');
        }
    }
    if ($alt) {
        $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
        $arr['url'] = $url;
        $arr['alt'] = $alt;
    } else {
        return $url;
    }
}
function get_thumb_alt($id = '') {
    if ($id == '') { $id = $post->ID; }
    $thumbnail_id = get_post_thumbnail_id( $post->ID );
    $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
    return $alt;
}
function get_bgimg_f($fname = '',$file = '',$size = 'url') {
    $bg = get_field($fname);
    if ($bg) {
        if ($size == 'url') {
            $bg = $bg[url];
        } else {
            $bg = $bg[sizes][$size];
        }
    } else {
        $bg = get_dir('images/'.$file);
    }
    return $bg;
}
function get_bgimg_fo($fname = '',$file = '',$size = 'url') {
    $bg = get_field($fname,'options');
    if ($bg) {
        if ($size == 'url') {
            $bg = $bg[url];
        } else {
            $bg = $bg[sizes][$size];
        }
    } else {
        $bg = get_dir('images/'.$file);
    }
    return $bg;
}
function get_dir($dir = 'nodir') {
    if ($dir == 'nodir') {
        $tdir = get_template_directory_uri().'/';
    } else {
        $tdir = get_template_directory_uri().'/'.$dir;
    }
    return $tdir;
}
function get_obj_img($obj,$size = 'medium') {
    $src = $obj[sizes][$size];
    $title = $obj[title];
    $img = '<img src="'.$src.'" alt="'.$title.'" />';
    return $img;
}
function y_video($iframe,$cp = 1,$auto = 0,$loop = 0,$id = '') {
    global $post;
    if (empty($id)) {
        $id = 'Video_'.$post->ID;
    }
    preg_match('/src="(.+?)"/', $iframe, $matches);
    $src = $matches[1];
    $params = array(
        'controls'  => $cp,
        'hd'        => 1,
        'rel'       => 0,
        'autohide'  => 1,
        'enablejsapi'  => 1,
        'autoplay'  => $auto,
        'loop'      => $loop,
        'volume'      => 0,
    );
    $new_src = add_query_arg($params, $src);
    $iframe = str_replace($src, $new_src, $iframe);
    $attributes = 'frameborder="0" id="'.$id.'" ';
    $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
    echo $iframe;
}
function svg($url,$class='',$field = '',$alt = '') {
    $nosvg = get_template_directory_uri().'/images/icons/nosvg.png';
    if ($field == 'field') {
        $url = get_field($url);
    } elseif ($field == 'options') {
        $url = get_field($url,'options');
    }
    if (is_array($url)) {
        if (empty($alt)) { $alt = $url[alt]; }
        $url = $url[url];
        $svg = $url;
    } else {
        $svg = $url;
    }
    $svg = explode('.', $svg);
    $csvg = count($svg);
    if ($csvg == 1) {
        $path = TEMPLATEPATH.'/images/'.$svg[0].'.svg';
        if (file_exists($path)) {
            $svg = file_get_contents($path);
            $svg = explode('<svg',$svg);
            $svg = '<svg class="'.$class.' anim" '.$svg[1];
        } else {
            $svg = '<img src="'.$nosvg.'" alt="" class="'.$class.'"/>';
        }
    } else {
        $place = $csvg - 1;
        if ($svg[$place] == 'svg') {
            $url = explode('/',$url);
            $c = count($url) - 1;
            $path = WP_CONTENT_DIR.'/uploads/'.$url[$c - 2].'/'.$url[$c - 1].'/'.$url[$c];
            $svg = file_get_contents($path);
            $svg = explode('<svg',$svg);
            $svg = '<svg class="'.$class.' anim" '.$svg[1];
        } else {
            $svg = '<img src="'.$url.'" alt="'.$alt.'" class="'.$class.'"/>';
        }
    }
    return $svg;
}

function c_page() {
    global $wp_query;
    $loop['id'] = $wp_query->queried_object_id;
    $loop['page'] = 'notfound';
    $loop['title'] = '';
    $loop['type'] = '';
    $loop['format'] = '';
    $loop['template'] = basename(get_page_template());
    $loop['is_front'] = 0;
    $loop['is_tax'] = 0;
    $loop['is_page'] = 0;
    $loop['is_single'] = 0;

    if ($wp_query->is_page) {
        if (is_front_page()) {
            $loop['page'] =  'front';
            $loop['is_front'] = 1;
        } else {
            $loop['page'] =  'page';
        }
        $loop['is_page'] = 1;
        $loop['type'] = $wp_query->queried_object->post_type;
        $loop['title'] = $wp_query->queried_object->post_title;
    } elseif ($wp_query->is_home) {
        $loop['page'] = 'home';
        $loop['is_page'] = 1;
        $loop['type'] = $wp_query->queried_object->post_type;
        $loop['title'] = $wp_query->queried_object->post_title;
    } elseif ($wp_query->is_single) {
        $loop['page'] = ($wp_query->is_attachment) ? 'attachment' : 'single';
        $loop['title'] = $wp_query->queried_object->post_title;
        $loop['type'] = $wp_query->queried_object->post_type;
        $loop['is_single'] = 1;
        $loop['format'] = get_post_format() ? : 'standard';
    } elseif ($wp_query->is_category) {
        $loop['page'] = 'category';
        $loop['title'] = $wp_query->queried_object->name;
        $loop['type'] = $wp_query->queried_object->taxonomy;
        $loop['is_tax'] = 1;
    } elseif ($wp_query->is_tag) {
        $loop['page'] = 'tag';
    } elseif ($wp_query->is_tax) {
        $loop['page'] = 'tax';
        $loop['title'] = $wp_query->queried_object->name;
        $loop['type'] = $wp_query->queried_object->taxonomy;
        $loop['is_tax'] = 1;
    } elseif ($wp_query->is_archive) {
        if ($wp_query->is_day) {
            $loop['page'] = 'day';
        } elseif ($wp_query->is_month) {
            $loop['page'] = 'month';
        } elseif ($wp_query->is_year) {
            $loop['page'] = 'year';
        } elseif ($wp_query->is_author) {
            $loop['page'] = 'author';
        } else {
            $loop['page'] = 'archive';
        }
    } elseif ($wp_query->is_search) {
        $loop['page'] = 'search';
        $loop['title'] = 'תוצאות חיפוש עבור: '.get_search_query();
    } elseif ($wp_query->is_404) {
        $loop['page'] = 'notfound';
    }
    return $loop;
}

function filter_site_upload_size_limit( $size ) {
    $size = 1024 * 2000;
    return $size;
}

add_filter( 'tiny_mce_before_init', 'short_save_tiny_mce_before_init' );
function short_save_tiny_mce_before_init($initArray) {
$initArray['setup'] = <<<JS
[function(ed) {
    var isCtrl = false;
    var isShift = false;
    ed.onKeyUp.add(function(ed, e) {
        if(e.keyCode == 17) {
            isCtrl = false;
        }
        if(e.keyCode == 16) {
            isShift = false;
        }
    });
    ed.onKeyDown.add(function(ed, e) {
        if(e.keyCode == 17) {
            isCtrl = true;
        }
        if(e.keyCode == 16) {
            isShift = true;
        }
        if(e.keyCode == 83 && isCtrl && isShift) {
            jQuery('#publish.button-primary').trigger('click');
        }
        //console.debug('Key down event: ' + e.keyCode);
    });
}][0]
JS;
    return $initArray;
}

add_filter( 'upload_size_limit', 'filter_site_upload_size_limit', 20 );

if ($options['set_products']) {
    function product_post_type() {

                $labels = array(
                            'name'                => 'מוצרים',
                            'singular_name'       => 'מוצרים',
                            'menu_name'           => 'מוצרים',
                            'parent_item_colon'   => 'פריט אב:',
                            'all_items'           => 'כל המוצרים',
                            'view_item'           => 'הצג מוצר',
                            'add_new_item'        => 'הוסף מוצר חדש',
                            'add_new'             => 'הוסף חדש',
                            'edit_item'           => 'ערוך מוצר',
                            'update_item'         => 'עדכון מוצר',
                            'search_items'        => 'חפש מוצר',
                            'not_found'           => 'לא נמצא',
                            'not_found_in_trash'  => 'לא מצא באשפה',
                );
                $rewrite = array(
                            'slug'                => 'product',
                            'with_front'          => true,
                            'pages'               => true,
                            'feeds'               => true,
                );
                $args = array(
                            'label'               => 'product',
                            'description'         => 'מוצרים',
                            'labels'              => $labels,
                            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
                            'taxonomies'          => array( 'product_cat' ),
                            'hierarchical'        => false,
                            'public'              => true,
                            'show_ui'             => true,
                            'show_in_menu'        => true,
                            'show_in_nav_menus'   => true,
                            'show_in_admin_bar'   => true,
                            'menu_position'       => 5,
                            'menu_icon'           => 'dashicons-products',
                            'can_export'          => true,
                            'has_archive'         => true,
                            'exclude_from_search' => false,
                            'publicly_queryable'  => true,
                            'rewrite'             => $rewrite,
                            'capability_type'     => 'post',
                );
                register_post_type( 'product', $args );

    }

    add_action( 'init', 'product_post_type', 0 );

    function product_taxonomy() {

                $labels = array(
                            'name'                       => 'קטגוריות מוצרים',
                            'singular_name'              => 'קטגוריות מוצרים',
                            'menu_name'                  => 'קטגוריות מוצרים',
                            'all_items'                  => 'כל הקטגוריות',
                            'parent_item'                => 'קטגורית הורה',
                            'parent_item_colon'          => 'קטגורית הורה:',
                            'new_item_name'              => 'שם קטגוריה חדשה',
                            'add_new_item'               => 'להוסיף קטגוריה חדשה',
                            'edit_item'                  => 'ערוך קטגוריה',
                            'update_item'                => 'עדכן קטגוריה',
                            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
                            'search_items'               => 'חיפוש קטגוריות',
                            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
                            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
                            'not_found'                  => 'לא נמצא',
                );
                $rewrite = array(
                            'slug'                       => 'product_cat',
                            'with_front'                 => true,
                            'hierarchical'               => false,
                );
                $args = array(
                            'labels'                     => $labels,
                            'hierarchical'               => true,
                            'public'                     => true,
                            'show_ui'                    => true,
                            'show_admin_column'          => true,
                            'show_in_nav_menus'          => true,
                            'show_tagcloud'              => true,
                            'rewrite'                    => $rewrite,
                );
                register_taxonomy( 'product_cat', array( 'product' ), $args );

    }

    add_action( 'init', 'product_taxonomy', 0 );
}
if ($options['set_projects']) {
    function project_post_type() {

                $labels = array(
                            'name'                => 'פרויקטים',
                            'singular_name'       => 'פרויקטים',
                            'menu_name'           => 'פרויקטים',
                            'parent_item_colon'   => 'פריט אב:',
                            'all_items'           => 'כל הפרויקטים',
                            'view_item'           => 'הצג פרויקט',
                            'add_new_item'        => 'הוסף פרויקט',
                            'add_new'             => 'הוסף פרויקט חדש',
                            'edit_item'           => 'ערוך פרויקט',
                            'update_item'         => 'עדכון פרויקט',
                            'search_items'        => 'חפש פריטים',
                            'not_found'           => 'לא נמצא',
                            'not_found_in_trash'  => 'לא מצא באשפה',
                );
                $rewrite = array(
                            'slug'                => 'project',
                            'with_front'          => true,
                            'pages'               => true,
                            'feeds'               => true,
                );
                $args = array(
                            'label'               => 'project',
                            'description'         => 'פרויקטים',
                            'labels'              => $labels,
                            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
                            'taxonomies'          => array( 'project_cat' ),
                            'hierarchical'        => false,
                            'public'              => true,
                            'show_ui'             => true,
                            'show_in_menu'        => true,
                            'show_in_nav_menus'   => true,
                            'show_in_admin_bar'   => true,
                            'menu_position'       => 5,
                            'menu_icon'           => 'dashicons-portfolio',
                            'can_export'          => true,
                            'has_archive'         => true,
                            'exclude_from_search' => false,
                            'publicly_queryable'  => true,
                            'rewrite'             => $rewrite,
                            'capability_type'     => 'post',
                );
                register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

                $labels = array(
                            'name'                       => 'קטגוריות פרויקטים',
                            'singular_name'              => 'קטגוריות פרויקטים',
                            'menu_name'                  => 'קטגוריות פרויקטים',
                            'all_items'                  => 'כל הקטגוריות',
                            'parent_item'                => 'קטגורית הורה',
                            'parent_item_colon'          => 'קטגורית הורה:',
                            'new_item_name'              => 'שם קטגוריה חדשה',
                            'add_new_item'               => 'להוסיף קטגוריה חדשה',
                            'edit_item'                  => 'ערוך קטגוריה',
                            'update_item'                => 'עדכן קטגוריה',
                            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
                            'search_items'               => 'חיפוש קטגוריות',
                            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
                            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
                            'not_found'                  => 'לא נמצא',
                );
                $rewrite = array(
                            'slug'                       => 'project_cat',
                            'with_front'                 => true,
                            'hierarchical'               => false,
                );
                $args = array(
                            'labels'                     => $labels,
                            'hierarchical'               => true,
                            'public'                     => true,
                            'show_ui'                    => true,
                            'show_admin_column'          => true,
                            'show_in_nav_menus'          => true,
                            'show_tagcloud'              => true,
                            'rewrite'                    => $rewrite,
                );
                register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}
if ($options['set_recs']) {
    function rec_post_type() {

                $labels = array(
                            'name'                => 'המלצות',
                            'singular_name'       => 'המלצות',
                            'menu_name'           => 'המלצות',
                            'parent_item_colon'   => 'פריט אב:',
                            'all_items'           => 'כל ההמלצות',
                            'view_item'           => 'הצג המלצה',
                            'add_new_item'        => 'הוסף המלצה חדשה',
                            'add_new'             => 'הוסף חדש',
                            'edit_item'           => 'ערוך את ההמלצה',
                            'update_item'         => 'עדכון המלצה',
                            'search_items'        => 'חפש המלצה',
                            'not_found'           => 'לא נמצא',
                            'not_found_in_trash'  => 'לא מצא באשפה',
                );
                $rewrite = array(
                            'slug'                => 'rec',
                            'with_front'          => true,
                            'pages'               => true,
                            'feeds'               => true,
                );
                $args = array(
                            'label'               => 'rec',
                            'description'         => 'המלצות',
                            'labels'              => $labels,
                            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
                            'taxonomies'          => array( 'rec_cat' ),
                            'hierarchical'        => false,
                            'public'              => true,
                            'show_ui'             => true,
                            'show_in_menu'        => true,
                            'show_in_nav_menus'   => true,
                            'show_in_admin_bar'   => true,
                            'menu_position'       => 5,
                            'menu_icon'           => 'dashicons-star-filled',
                            'can_export'          => true,
                            'has_archive'         => true,
                            'exclude_from_search' => false,
                            'publicly_queryable'  => true,
                            'rewrite'             => $rewrite,
                            'capability_type'     => 'post',
                );
                register_post_type( 'rec', $args );

    }
    add_action( 'init', 'rec_post_type', 0 );

    function rec_taxonomy() {
                $labels = array(
                            'name'                       => 'קטגורית המלצות',
                            'singular_name'              => 'קטגורית המלצות',
                            'menu_name'                  => 'קטגורית המלצות',
                            'all_items'                  => 'כל הקטגוריות',
                            'parent_item'                => 'קטגורית הורה',
                            'parent_item_colon'          => 'קטגורית הורה:',
                            'new_item_name'              => 'שם קטגוריה חדשה',
                            'add_new_item'               => 'להוסיף קטגוריה חדשה',
                            'edit_item'                  => 'ערוך קטגוריה',
                            'update_item'                => 'עדכן קטגוריה',
                            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
                            'search_items'               => 'חיפוש קטגוריות',
                            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
                            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
                            'not_found'                  => 'לא נמצא',
                );
                $rewrite = array(
                            'slug'                       => 'rec_cat',
                            'with_front'                 => true,
                            'hierarchical'               => false,
                );
                $args = array(
                            'labels'                     => $labels,
                            'hierarchical'               => true,
                            'public'                     => true,
                            'show_ui'                    => true,
                            'show_admin_column'          => true,
                            'show_in_nav_menus'          => true,
                            'show_tagcloud'              => true,
                            'rewrite'                    => $rewrite,
                );
                register_taxonomy( 'rec_cat', array( 'rec' ), $args );
    }
    add_action( 'init', 'rec_taxonomy', 0 );
}
if ($options['set_cncpt'] != '') {
    function cncpt_post_type() {
    global $options;
    $cptname = $options['set_cncpt'];

                $labels = array(
                            'name'                => $cptname,
                            'singular_name'       => $cptname,
                            'menu_name'           => $cptname,
                            'parent_item_colon'   => __( 'פריט אב:', 'text_domain' ),
                            'all_items'           => __( 'כל ה'.$cptname, 'text_domain' ),
                            'view_item'           => __( 'הצג פריט', 'text_domain' ),
                            'add_new_item'        => __( 'הוסף פריט חדש', 'text_domain' ),
                            'add_new'             => __( 'הוסף חדש', 'text_domain' ),
                            'edit_item'           => __( 'ערוך פריט', 'text_domain' ),
                            'update_item'         => __( 'עדכון פריט', 'text_domain' ),
                            'search_items'        => __( 'חפש פריטים', 'text_domain' ),
                            'not_found'           => __( 'לא נמצא', 'text_domain' ),
                            'not_found_in_trash'  => __( 'לא מצא באשפה', 'text_domain' ),
                );
                $rewrite = array(
                            'slug'                => 'cncpt',
                            'with_front'          => true,
                            'pages'               => true,
                            'feeds'               => true,
                );
                $args = array(
                            'label'               => $cptname,
                            'description'         => $cptname,
                            'labels'              => $labels,
                            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
                            'taxonomies'          => array( 'cncpt_cat' ),
                            'hierarchical'        => false,
                            'public'              => true,
                            'show_ui'             => true,
                            'show_in_menu'        => true,
                            'show_in_nav_menus'   => true,
                            'show_in_admin_bar'   => true,
                            'menu_position'       => 5,
                            'menu_icon'           => 'dashicons-star-filled',
                            'can_export'          => true,
                            'has_archive'         => true,
                            'exclude_from_search' => false,
                            'publicly_queryable'  => true,
                            'rewrite'             => $rewrite,
                            'capability_type'     => 'post',
                );
                register_post_type( 'cncpt', $args );

    }

    add_action( 'init', 'cncpt_post_type', 0 );

    function cncpt_taxonomy() {

                $labels = array(
                            'name'                       => _x( 'קטגוריות '.$cptname, 'Taxonomy General Name', 'text_domain' ),
                            'singular_name'              => _x( 'קטגורית '.$cptname, 'Taxonomy Singular Name', 'text_domain' ),
                            'menu_name'                  => __( 'קטגורית '.$cptname, 'text_domain' ),
                            'all_items'                  => __( 'כל הקטגוריות', 'text_domain' ),
                            'parent_item'                => 'קטגורית הורה',
                            'parent_item_colon'          => 'קטגורית הורה:',
                            'new_item_name'              => __( 'שם קטגוריה חדשה', 'text_domain' ),
                            'add_new_item'               => __( 'להוסיף קטגוריה חדשה', 'text_domain' ),
                            'edit_item'                  => __( 'ערוך קטגוריה', 'text_domain' ),
                            'update_item'                => __( 'עדכן קטגוריה', 'text_domain' ),
                            'separate_items_with_commas' => __( 'קטגוריות נפרדות עם פסיק', 'text_domain' ),
                            'search_items'               => __( 'חיפוש קטגוריות', 'text_domain' ),
                            'add_or_remove_items'        => __( 'להוסיף או להסיר קטגוריות', 'text_domain' ),
                            'choose_from_most_used'      => __( 'בחר מהקטגוריות הנפוצות ביותר', 'text_domain' ),
                            'not_found'                  => __( 'לא נמצא', 'text_domain' ),
                );
                $rewrite = array(
                            'slug'                       => 'cncpt_cat',
                            'with_front'                 => true,
                            'hierarchical'               => false,
                );
                $args = array(
                            'labels'                     => $labels,
                            'hierarchical'               => true,
                            'public'                     => true,
                            'show_ui'                    => true,
                            'show_admin_column'          => true,
                            'show_in_nav_menus'          => true,
                            'show_tagcloud'              => true,
                            'rewrite'                    => $rewrite,
                );
                register_taxonomy( 'cncpt_cat', array( 'cncpt' ), $args );

    }

    add_action( 'init', 'cncpt_taxonomy', 0 );
}

function yvideo_shortcode( $atts ) {
	$atts = shortcode_atts(
		array(
			'src' => '',
			'ap' => '0',
			'rel' => '0',
			'loop' => '0',
			'mute' => '0',
		),
		$atts,
		'YouTube'
	);
    $src = $atts['src'];
    $autoplay = $atts['ap'];
    $rel = $atts['rel'];
    $mute = $atts['mute'];
    $loop = $atts['loop'];
    $temp = explode('watch?v=',$src);
    $src = $temp[0].'embed/'.$temp[1];
    $vid = $temp[1];
    if ($loop) {
        $embed = '<iframe width="100%" height="315" class="anim resize" data-ratio="16:9" src="'. $src .'?version=3&autoplay='. $autoplay .'&rel='. $rel .'&loop='. $loop .'&playlist='. $vid .'&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
    } else {
        $embed = '<iframe width="100%" height="315" class="anim resize" data-ratio="16:9" src="'. $src .'?version=3&autoplay='. $autoplay .'&rel='. $rel .'&enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
    }
    return $embed;
}
add_shortcode( 'YouTube', 'yvideo_shortcode' );

function tmce_yt_btn() {
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'tmce_yt_btn_plugin' );
		add_filter( 'mce_buttons', 'tmce_yt_btn_register' );
	}
}
add_action('admin_head', 'tmce_yt_btn');

function tmce_yt_btn_plugin( $plugin_array ) {
	$plugin_array['y_video_button'] = get_template_directory_uri() .'/js/buttons.js';
	return $plugin_array;
}

function tmce_yt_btn_register( $buttons ) {
	array_push( $buttons, 'y_video_button' );
	return $buttons;
}

function my_tiny_mce_before_init( $in ) {

    // customize the buttons
    $in['toolbar1'] = 'bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,unlink,table,fullscreen,undo,redo,wp_adv,spellchecker,dfw,y_video_button,ltr';
    $in['toolbar2'] = 'fontselect,fontsizeselect,formatselect,alignjustify,strikethrough,outdent,indent,pastetext,removeformat,charmap,wp_more,emoticons,forecolor,wp_help';

    // Debug:
    //print_r( $in );
    // exit();

    // Keep the "kitchen sink" open:
    $in[ 'wordpress_adv_hidden' ] = FALSE;

    return $in;
}
add_filter( 'tiny_mce_before_init', 'my_tiny_mce_before_init' );

function my_acf_init() {
    global $options;
    $key = $options['gmap_key'];
	acf_update_setting('google_api_key', $key);
}
add_action('acf/init', 'my_acf_init');

function my_acf_google_map_api( $api ){
    global $options;
    $key = $options['gmap_key'];
	$api['key'] = $key;
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function wpse_188863_get_allowed_roles( $user ) {
    $allowed = array();

    if ( in_array( 'administrator', $user->roles ) ) {
        $allowed = array_keys( $GLOBALS['wp_roles']->roles );
    } elseif ( in_array( 'leos_client', $user->roles ) ) {
        $allowed[] = 'contributor';
    }
    return $allowed;
}

function wpse_188863_editable_roles( $roles ) {
    if ( $user = wp_get_current_user() ) {
        $allowed = wpse_188863_get_allowed_roles( $user );

        foreach ( $roles as $role => $caps ) {
            if ( ! in_array( $role, $allowed ) )
                unset( $roles[ $role ] );
        }
    }
    return $roles;
}
add_filter( 'editable_roles', 'wpse_188863_editable_roles' );

function wpse_188863_map_meta_cap( $caps, $cap, $user_ID, $args ) {
    if ( ( $cap === 'edit_user' || $cap === 'delete_user' ) && $args ) {
        $the_user = get_userdata( $user_ID );
        $user     = get_userdata( $args[0] );

        if ( $the_user && $user && $the_user->ID != $user->ID) {
            $allowed = wpse_188863_get_allowed_roles( $the_user );

            if ( array_diff( $user->roles, $allowed ) ) {
                $caps[] = 'not_allowed';
            }
        }
    }

    return $caps;
}
if (!is_admin()) {
    add_filter( 'map_meta_cap', 'wpse_188863_map_meta_cap', 10, 4 );
}

$fuckoffOptions = get_option('coder-limit-login-options');

if($fuckoffOptions){
    $fuckoffOptions['coder_already_login_message'] = '<div style="text-align: center"><img src="'.get_template_directory_uri().'/images/keep-calm.png" style="max-width: 100%;" alt=""></div>';
    $fuckoffOptions['coder_force_logout_message'] = '<div style="text-align: center"><img src="'.get_template_directory_uri().'/images/keep-calm.png" style="max-width: 100%;" alt=""></div>';
    update_option('coder-limit-login-options', $fuckoffOptions);
}

function term_check($term) {
    if (is_numeric($term)) {
        $return['tax'] = 'category';
        $return['term_id'] = $term;
    } else {
        if (strpos($term, '_') !== FALSE && !is_object($term)) {
            $temp = explode('_',$term);
            $return['term_id'] = end($temp);
            if (count($temp) > 2) {
                unset($temp[count($temp) - 1]);
                $return['tax'] = implode('_',$temp);
            } else {
                $return['tax'] = $temp[0];
            }
        } elseif (is_object($term)) {
            if ($term->taxonomy != 'category') {
                $return['post_type'] = str_replace('_cat','',$term->taxonomy);
            }
            $return['tax'] = $term->taxonomy;
            $return['term_id'] = $term->term_id;
        }
    }
    return $return;
}

function get_the_posts($q_object = array(), $tax_q = '') {
    $posts_per_page = -1;
    $post_type = 'post';
    $category = '';
    $orderby = 'date';
    $order = 'DESC';
    $include = '';
    $exclude = '';
    $post_status = 'publish';
    $suppress_filters = true;

    $args = array(
        'posts_per_page'   => $posts_per_page,
        'offset'           => 0,
        'category'         => $category,
        'orderby'          => $orderby,
        'order'            => $order,
        'include'          => $include,
        'exclude'          => $exclude,
        'post_type'        => $post_type,
        'post_status'      => $post_status,
        'suppress_filters' => $suppress_filters
    );
    if (!empty($q_object)) {
        foreach($q_object as $key => $val) {
            if (array_key_exists($key,$q_object)) {
                $args[$key] = $val;
            }
            if ($key == 'orderby' && $val == 'rand') {
                $args['order'] = '';
            }
        }
    }
    if (!empty($tax_q)) {
        //$tax_q = self::term_check($tax_q);
        $tax = $tax_q['tax'];
        $t_id = $tax_q['term_id'];

        if ($tax_q['post_type']) { $args['post_type'] = $tax_q['post_type']; }
        if (!empty($tax)){
            $tax_args = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => $tax,
                        'field' => 'term_id',
                        'terms' => $t_id
                    )
                )
            );
            $args = $args + $tax_args;
        }
    }
    $q_posts = get_posts($args);
    return $q_posts;
}

function get_term_childs($term, $type = 'id', $options = '', $deep = 1) {
    /*if (empty(self::$term_childs_depp)) {
        self::$term_childs_depp = 1;
    }*/
    //$tax = self::term_check($term);
    $taxonomy = $tax['tax'];
    $term_id = $tax['term_id'];
    if (strpos($options, ',') !== FALSE) {
        $options = explode(',',$options);
    }
    if (!taxonomy_exists($taxonomy)) {
        return new WP_Error( 'invalid_taxonomy', __( 'Invalid taxonomy.' ) );
    }

    $term_id = intval($term_id);

    $terms = _get_term_hierarchy($taxonomy);

    if (!isset($terms[$term_id]))
        return array();

    $children = $terms[$term_id];

    $i = 0; foreach ( (array) $terms[$term_id] as $child ) {
        if ( $term_id == $child ) {
            continue;
        }

        if ($type == 'object') {
            $child_obj = get_term($child, $taxonomy);
            if ($options == 'url' || $options == 'link' || in_array('url',$options) || in_array('link',$options)) {
                $child_obj->url = get_term_link($child_obj);
            }
            if ($options == 'fields' || in_array('fields',$options)) {
                $child_obj->fields = get_fields($child_obj);
            }
            if ($options == 'thumb' || in_array('thumb',$options)) {
                $child_obj->thumb = get_field('thumb',$child_obj);
            }
            $children[$i] = $child_obj;
            /*if (isset($terms[$child]) && self::$term_childs_depp < $deep) {
                $children = array_merge($children, self::get_term_childs($taxonomy.'_'.$child,'object'));
            } */
        } else {
            /*if (isset($terms[$child]) && self::$term_childs_depp < $deep) {
                $children = array_merge($children, self::get_term_childs($taxonomy.'_'.$child));
            } */
        }
        $i++;
    }
    //self::$term_childs_depp++;
    return $children;
}

function loadScripts(){
    wp_enqueue_script('framework-script', get_template_directory_uri() . '/dist/js/scripts.min.js', false, '', true);
    wp_enqueue_style('framework-style', get_template_directory_uri() . '/dist/css/main.min.css');
}

add_action('wp_enqueue_scripts', 'loadScripts');

?>
