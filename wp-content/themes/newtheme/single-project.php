<?php get_header(); global $my_lang,$isMobile;
$terms = get_the_terms($post->ID, 'project_cat');
if ($terms) { foreach ($terms as $term) { $myarr[] = $term->term_id; } } ?>
<div id="SingleProjectPage">
    <div class="container">
		<h1><?php the_title(); ?></h1>
		<div class="entry">
            <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<?php the_content(); ?>
		</div>
    </div>
</div>
<?php get_footer(); ?>
