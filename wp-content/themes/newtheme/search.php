<?php get_header(); ?>
	<?php if (have_posts()) : ?>
		<?php echo get_template_part('archive'); ?>
	<?php else : ?>
			<div class="container">
				<div class="title">
					<h3>לא נמצאו תוצאות חיפוש</h3>
				</div>
			</div>
	<?php endif; ?>
<?php get_footer(); ?>
