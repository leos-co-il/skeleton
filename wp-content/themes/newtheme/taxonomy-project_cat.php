<?php get_header();
$queried_object = get_queried_object();
$term_id = $queried_object->term_id; ?>
<div id="ProjectsArchPage">
	<div class="container">
		<?php if ( function_exists('yoast_breadcrumb') )
		{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
		<h1><?php single_cat_title(); ?></h1>
		<div class="Projectsbox inrowf">
			<?php while (have_posts()) : the_post(); ?>
			<div class="oneproject col3">
			    <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			    <div class="entry">
			    	<?php the_excerpt(); ?>
			    </div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>