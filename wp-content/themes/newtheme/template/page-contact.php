<?php
/*
Template Name: צור קשר
*/
global $my_lang,$phone,$mail,$address,$socials;
global $options;
$key = $options['gmap_key'];
get_header(); ?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo $key; ?>"></script>
<div class="container">
    <div class="post">
        <div class="entry">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <?php echo do_shortcode('[contact-form-7 id="4" title="צור קשר"]'); ?>
        <?php if (!empty($address)) { ?>
            <div class="map_wrap">
                <div class="acf-map" data-icon="<?php the_field('google_map_icon','options'); ?>">
                    <div class="marker" data-lat="<?php echo $address['lat']; ?>" data-lng="<?php echo $address['lng']; ?>"></div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
