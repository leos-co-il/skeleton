<?php
/*
Template Name: עמוד בית
*/
get_header(); ?>
<?php $bgs=get_field('topimages','options'); if ($bgs) {?>
    <div class="topimages">
    <?php foreach ($bgs as $bg) {?>
        <div class="norpt bgscov flex fcc aic" style="min-height:700px;background-image:url('<?php echo $bg[image];?>')">
            <h2 class="hfs60 bold center"style="color:<?php echo $bg[color];?>"><?php echo $bg[text];?></h2>
        </div>
    <?php }?>
</div>
<?php }?>
<?php $aboutpar=get_field('about_p','options'); if($aboutpar) {?>
    <?php $img=get_field('about_pic','options');?>
<div class="ABOUTUS bgimg bgscon bgpr"style="background-image:url('<?php echo $img[url];?>')">
    <div class="container c16 pdgtb30 flex aic ">
        <div class="w40"></div>
        <div class="w55 margin">
            <h2 class="hfs50 bold"><?php echo the_field('about_t','options');?></h2>
            <div class="entry">
                <?php echo $aboutpar;?>
            </div>
        </div>
    </div>
</div>
<?php }?>
<?php $whyus=get_field('why_us','options'); if ($whyus) {?>
    <div class="WHYSUS flex aic">
        <div class="w45 margin">
            <h2 class="hfs30 pdgb40"><?php echo the_field('whyus_t','options');?></h2>
            <?php foreach ($whyus as $reason) {?>
                <div class="flex aic pdgtb15">
                    <div class="ic bgimg bgscon circlebox  " style="height: 70px;width: 70px;background-image:url('<?php echo $reason[icon];?>')"></div>
                    <div class="w80 hfs25 light margin"><?php echo $reason[reason];?></div>
                </div>
            <?php }?>
        </div>
        <div class="bgimg w50 " style="min-height: 550px;background-image:url('<?php echo the_field('whyus_pic','options');?>')">
        </div>
    </div>
<?php }?>
<?php $articles=get_field('articles','options'); if($articles) {?>
<div class="ARTICLES pdgtb80 container c13 mpb10">
    <h2 class="sc2 center hfs50 bold"><a href="<?php ?>"><?php echo the_field('articles_t','options');?></a></h2>
        <div class="articles-wrap inrowf pdgtb80">
            <?php $i=0; $z=1;foreach ($articles as $article) { $id=$article->ID;?>
                <a data-wow-delay="<?php echo $i.'s';?>" class="col4 wow <?php if ($z%2) { echo 'fadeInUp' ;} else echo 'fadeInDown';?>" href="<?php echo get_the_permalink($id);?>">
                    <div class="bgimg w85 margin"style="height: 225px;background-image:url('<?php echo get_thumb_url('',$id);?>')"></div>
                    <h3 class="hfs21  pdgtb15 light center" style="color:#363636;"><?php echo get_the_title($id);?></h3>
                    <div class="hfs17 light center pdgb15" style="color:#959595"><?php echo trunc($article->post_content,5);?></div>
                    <div class="readmore center w85 margin hvr-shutter-out-horizontal"><?php echo "פירוט נוסף >"?></div>
                </a>
            <?php $i+=0.5;$z++;}?>
        </div>
</DIV>
<?php }?>
<?php $middleform=get_field('middleform','options'); if($middleform) {?>
<div class="MIDDLEFORM bgimg bgpb flex aic overh mbh400"style="height:700px;background-image:url('<?php echo the_field('middlef_img','options');?>')">
    <div class="w50 wdth10 nomobile dnone wd40">
    </div>
    <div class="w50 margin wow swing mw90 wdth100 wid80 wd60">
        <h2 class="wcolor hfs70 mhfs35 fnts40 center mw100 w70 bold margin"><?php echo the_field('middlef_t','options');?></h2>
        <h3 class="wcolor hfs25 light center"><?php echo the_field('middlef_st','options');?></h3>
        <div class="w65  pdgtb20 margin mw100 "><?php echo the_field('middleform','options');?></div>
    </div>
</div>
<?php }?>
<?php $recs=get_field('recs','options'); if ($recs) {?>
<div class="flex mblock">
    <div class="w50 mw100 bgimg flex aic fcc" style="min-height: 615px;background-image:url('<?php echo the_field('recs_bg','options');?>')">
            <div class="w85 mw90 mpdt30 padit30 mpb30 padt20">
                <h2 class=" mhfs40  center hfs50"><a href=""><?php echo the_field('recs_t','options');?></a></h2>
                <div class="recs ">
                <?php foreach ($recs as $rec ) { $id=$rec->ID;?>
                    <div class="rec relative">
                        <h2 class="hfs30 bold pdgb20"><?php echo get_the_title($id);?></h2>
                        <div class=" fnts21 center mhfs21 hfs25 light lh15 pdgtb80"><?php echo $rec->post_content;;?></div>
                    </div>
                <?php }?>
                </div>
            </div>
    </div>
    <div class="w50 mw100 bgimg"style="background-image:url('<?php echo the_field('recs_pic','options');?>')">
    </div>
</div>
<?php }?>
<?php $slider=get_field('pro_slider','options'); if ($slider) {?>
    <div class="PROMO  mblock  flex  aic overh" >
        <div class="w50 bgimg"style="min-height: 500px;background-image:url('<?php echo the_field('pro_img','options');?>')"></div>
        <div class="slider1 w50">
            <?php foreach ($slider as $slide) {?>
                <div class="flex aic  ">
                    <div class=" padit30 wid40  margin  mw90 wdth70 padt20 ">
                        <div class=" mpdt30  mcenter ">
                            <h2 class="hfs45 mhfs35 "style="color:#313232;"><?php echo $slide[title];?></h2>
                            <div class="entry pdgtb30  "><?php echo $slide[par];?></div>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
<?php }?>
<?php get_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
<script>
$(document).ready(function() {
    $('.topimages').slick({
      rtl:true,
      slidesToShow:1,
      pauseOnHover:false,
      autoplay:true,
      dots:false,
      fade:true,
      speed:3000,
      autoplaySpeed: 3000,
});
});
</script>
