<?php
/*
Template Name:אודות
*/
 get_header(); ?>
<div id="ThePage">
    <?php $top=get_field('pages_topimage') ? get_field('pages_topimage') : get_field('default','options');  ?>
        <div class="topimage bgimg relative  " style="  background-image:url('<?php echo $top;?>')">
            <div class="h100 w100 flex aife fcc mmnh350" style="min-height:500px;background-color:rgba(0, 32, 72,0.3);">
                <h1 class="hfs100 wcolor  bold center pdgb80"><?php echo the_title();?></h1>
            </div>
        </div>
	<div class="container">
		<div class="entry">
			<?php the_content(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
