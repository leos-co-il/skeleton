<?php Namespace WordPress\Plugin\GalleryManager;

abstract class AJAX_Requests {

  static function init(){
    self::registerAJAXHook('get_gallery', 'getGallery');
  }

  static function registerAJAXHook($action, $method){
    Add_Action("wp_ajax_{$action}", Array(__CLASS__, $method));
    Add_Action("wp_ajax_nopriv_{$action}", Array(__CLASS__, $method));
  }

  static function sendResponse($response){
    Header('Content-Type: application/json');
    Echo JSON_Encode($response);
    Exit;
  }

  static function getGallery(){
    $gallery_id = Trim($_REQUEST['gallery_id']);
    $gallery = new Gallery($gallery_id);
    $arr_images = $gallery->getImages();
    $arr_images = Array_Values($arr_images);

  	if (Empty($arr_images)) return False;

    foreach ($arr_images as &$image){
      $image = (Object) Array(
        'title' => IsSet($image->post_title) ? $image->post_title : False,
        'description' => IsSet($image->post_content) ? $image->post_content : False,
        'href' => $image->url,
        'thumbnail' => IsSet($image->thumbnail->url) ? $image->thumbnail->url : False
      );
    }

    # return the images
    self::sendResponse($arr_images);
  }

}

AJAX_Requests::init();
