<?php Namespace WordPress\Plugin\GalleryManager;

abstract class Scripts {

  static function init(){
    Add_Action('wp_enqueue_scripts', Array(__CLASS__, 'enqueueScripts'));
    Add_Action('admin_init', Array(__CLASS__, 'registerAdminScripts'));
  }

  static function enqueueScripts(){
    $arr_options = Options::Get();
    Unset($arr_options['disable_update_notification'], $arr_options['update_username'], $arr_options['update_password']);
    $arr_options['ajax_url'] = Admin_Url('admin-ajax.php');

    WP_Register_Script('gallery-manager', Core::$base_url . '/assets/js/gallery-manager.js', Array('jquery'), Core::version, Options::Get('script_position') != 'header');
    WP_Localize_Script('gallery-manager', 'GalleryManager', $arr_options);

    if (Options::get('lightbox')) WP_Enqueue_Script('gallery-manager');
  }

  static function registerAdminScripts(){
    WP_Register_Script('dynamic-gallery', Core::$base_url . '/assets/js/dynamic-gallery.js', Array('jquery'), Null, True);
    WP_Register_Script('gallery-meta-boxes', Core::$base_url . '/meta-boxes/meta-boxes.js', Array('jquery', 'dynamic-gallery'), Core::version, True);
  }

}

Scripts::init();
